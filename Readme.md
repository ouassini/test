# GitLab CI template for SonarQube

This project implements a generic GitLab CI template for running [SonarQube](https://www.sonarqube.org/) analysis.

SonarQube is a **Code Quality and Security tool** that helps you analyse your source code and detect quality issues or
security vulnerabilities as early as possible.

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'gitlab-ci-templates/sonar'
    ref: '1.0.0'
    file: '/templates/gitlab-ci-sonar.yml'
```

:warning: depending on your needs and environment, you might have to use [one of the template variants](#variants).


## SonarQube analysis job

This job performs a SonarQube analysis of your code.

It is bound to the `test` stage, and uses the following variables:

| Name                     | description                     | default value |
| ------------------------ | ------------------------------- | ----------------------------- |
| `SONAR_SCANNER_IMAGE`    | The Docker image used to run [sonar-scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) | `sonarsource/sonar-scanner-cli:latest` |
| `SONAR_URL`              | SonarQube server url            | _none_ (disabled) |
| :lock: `SONAR_AUTH_TOKEN`| SonarQube authentication [token](https://docs.sonarqube.org/latest/user-guide/user-token/) (depends on your authentication method) | _none_ |
| :lock: `SONAR_LOGIN`     | SonarQube login (depends on your authentication method)                | _none_ |
| :lock: `SONAR_PASSWORD`  | SonarQube password (depends on your authentication method)             | _none_ |
| `SONAR_BASE_ARGS`        | SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/) | `-Dsonar.host.url=${SONAR_URL} -Dsonar.projectKey=${CI_PROJECT_PATH_SLUG} -Dsonar.projectName=${CI_PROJECT_PATH} -Dsonar.projectBaseDir=. -Dsonar.links.homepage=${CI_PROJECT_URL} -Dsonar.links.ci=${CI_PROJECT_URL}/-/pipelines -Dsonar.links.issue=${CI_PROJECT_URL}/-/issues -Dsonar.branch.name=${CI_COMMIT_REF_NAME}` |
| :lock: `SONAR_GITLAB_TOKEN` | GitLab [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `api` scope. When set, activates the [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties) integration. | _none_ |
| `SONAR_GITLAB_ARGS`      | Extra arguments to use with [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties) | `-Dsonar.gitlab.url=${CI_SERVER_URL} -Dsonar.gitlab.user_token=${SONAR_GITLAB_TOKEN} -Dsonar.gitlab.project_id=${CI_PROJECT_ID} -Dsonar.gitlab.commit_sha=${CI_COMMIT_SHA} -Dsonar.gitlab.ref_name=${CI_COMMIT_REF_NAME}` |
| `SONAR_AUTO_ON_DEV_DISABLED` | When set, SonarQube analysis becomes **manual** on development branches (automatic otherwise) | _none_ |

### About branch analysis

As you can see, default SonarQube analysis arguments uses the [Branch Analysis](https://docs.sonarqube.org/latest/branches/overview/) 
feature (`sonar.branch.name` argument).

This is a great SonarQube feature but it assumes one of the following conditions:

* you are using a [Developer Edition](https://www.sonarqube.org/developer-edition/) version,
* or you are using Community Edition with an opensource plugin emulating the Branch Analysis feature:
    * etiher [sonar-branch-community](https://github.com/msanez/sonar-branch-community),
    * or [sonarqube-community-branch-plugin](https://github.com/mc1arke/sonarqube-community-branch-plugin),
    * ...

If you're not in those cases, then the SonarQube analysis will fail with default parameters. You'll have to override the
default `SONAR_BASE_ARGS` and disable it by removing the `sonar.branch.name` argument.

### About Sonar GitLab plugin

The [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin) uses the GitLab APIs to inline comments 
into your commits directly in GitLab for each new anomaly.

As explained above, this template automatically enables the Sonar GitLab plugin if `SONAR_GITLAB_TOKEN` is set. 
It will then simply append the `SONAR_GITLAB_ARGS` (overridable) to the SonarQube analysis arguments.

Comments added to GitLab will appear as owned by the user associated to the GitLab [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

### How should I configure other SonarQube arguments ?

The simplest and recommended way to configure other SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/)
(and even [language specific args](https://docs.sonarqube.org/latest/analysis/languages/overview/)) is to use a 
`sonar-project.properties` file at the root of your repository.


## Using the Forge-as-a-Service (FaaS)

Accessing a SonarQube server from the [FaaS](https://faas.forge.orange-labs.fr/documentation/master/userguide/index.html#_sonarqube_analyse_de_qualit%C3%A9_de_code) 
can be done with the [API gateway](https://faas.forge.orange-labs.fr/documentation/master/userguide/index.html#gateway-api).

Technically it requires to:

1. set the `SONAR_URL` variable with your SONAR url:
    * `https://faas-gw.innov.intraorange/my-project/sonar` if you're accessing from the GIN networks (GRaaS runners - _the default_)
    * `https://my-project.faas-gw.rd.francetelecom.fr/sonar` or `https://faas-gw.rd.francetelecom.fr/my-project/sonar` 
      if you're accessing from RSC or Innovation networks (DevOps Store runners)
2. [generate a user access token](https://docs.sonarqube.org/latest/user-guide/user-token/) in SONAR,
3. set it as the `SONAR_AUTH_TOKEN` secret variable.

:warning: you will also have to declare the trusted Orange CA certificates as FaaS server certificate is issued by _Orange Internal G2 Server CA_.
That can be done simply by declaring the following variable in your `.gitlab-ci.yml` file:

```yaml
variables:
  # Orange CA certificates
  CUSTOM_CA_CERTS: |
    -----BEGIN CERTIFICATE-----
    MIIFsDCCA5igAwIBAgIBADANBgkqhkiG9w0BAQ0FADBdMQswCQYDVQQGEwJGUjEP
    MA0GA1UECgwGT3JhbmdlMRgwFgYDVQQLDA9GUiA4OSAzODAxMjk4NjYxIzAhBgNV
    BAMMGk9yYW5nZSBJbnRlcm5hbCBHMiBSb290IENBMB4XDTE2MDUwMjExMzgwN1oX
    DTQxMDUwMjExMzgwN1owXTELMAkGA1UEBhMCRlIxDzANBgNVBAoMBk9yYW5nZTEY
    MBYGA1UECwwPRlIgODkgMzgwMTI5ODY2MSMwIQYDVQQDDBpPcmFuZ2UgSW50ZXJu
    YWwgRzIgUm9vdCBDQTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAOCL
    6xok2VzVyHsl9RucKjtEALfI1Cx9uu1AbflfsVP4YEpOHtDW4yfr39uSB9NvjVRJ
    8LPteLbsuCHaaDM6D4urluhjS73C2vorMEElgCCG1sjHa0pXDLVG7H9CegqlyZtn
    vbzquJbYsCZaywBYx4pVoe0JvPJKI7/nvE2IdI0Ue9Pr4v0nUiJhzZG1S04mDw4E
    MlTvW60lFLqnqXDA0itIJfpAwbAFqw1xsvFxXBnC/vUUbQctOi7je8YC/p5w0aoJ
    o2wGkgDw2bGpO1VaeyfxTtyf41RUC2wuV9bO2vhV1aWf0vp59zybMlL/pMTmiaoa
    rVQr8aE/RGujn/mMqzT4JVmiCmD5bPy2OVKJnch7L/RNI/LropF17lPzpXboqk8M
    2K2+T7AxIJ0voanaxPXdZFElhxCg/XW04OaVrN4dO1x3njjR2MNizFhe79NZ7LEd
    efltcg11IXC6dKtCgs0/GLu6I6nYuSLRq6N5nwAs017IJw1RSFT2SYS6g6JjiAT+
    mr3dqXBSsNwRBug8oPxm82VNUHNcj/LqNYr8EEDqICd8PtRbEA6pSXMzxqO2zcRE
    DprWOUpL47Y5CKpcG5R+stezHgXM8ArsmT2A05+CQ3vey6pp/10q80gZ11iJDIqv
    GVVQvcvBqQ+0Eo2Rr5Kv9lPbzIvshxr6gSeEHo8nAgMBAAGjezB5MA8GA1UdEwEB
    /wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMB0GA1UdDgQWBBTGON+k2onDjBtKS2UH
    Ja+z1LZHgjAfBgNVHSMEGDAWgBTGON+k2onDjBtKS2UHJa+z1LZHgjAWBgNVHSAE
    DzANMAsGCSqBegEQDAoBATANBgkqhkiG9w0BAQ0FAAOCAgEAHm5FZlDv+2pzV7f4
    hfEDYWwFn2Lbqyn/Y8BsiyfvTX11AYOw41OeFLrk3FFM6+iLb14DXQcTwHjBux+2
    htH4T3jyPeBmsnRzuxsb61TqYO1TnId8zzroyPTT+MHABmETyUf9uuSispNgJJXk
    YYxpVPVLUZNtSuoDuVkjHJOQ0NRTB+1XzfShuSwI4vtttlhRg6qVvgjnyP2tR8BY
    wkHWI+iHM8VUwoZYIyk3zRbFTdqWEyebUAm2aJ7LguroMTZ71ZxvD/LJfHybj8Hb
    rm2Tk+Dq5gvBiggfgs23lx2uWBJbMa8F7I7JH/+YsvwXmlQSkDPDcUopKestiKVb
    fAhY1NRDqMlwkOLLrGxPjAKCDYwVkxz0pkLnWDiDRNpLlDpJ6x3+dOX34vi+K3dq
    wl73JXdXo4NEXswVsVM8fD+cdHluS74pemMf/7olUfd60YYC+6RwQRyP/4I0WaNs
    6D4B8R3rc/X1obgvx6NfF55mFEwW3yhxRLjux5sYEBfkT8tTllVcRUwJCQumLUS9
    rnrCMsDkBfI/ZN+ITBcWi6cAyOcsIOk4up9aUvNJTMKUlEybDNN8cDp9+vLonN2h
    YMg1IfJ+IZs6x3rDr5MXuWTks34hEJidpmOTmOyy/QPHCK0BGRmUoYJr4CHS4FL2
    BtsnCPyUUO4gppjywfYrotQ2gTQ=
    -----END CERTIFICATE-----
    -----BEGIN CERTIFICATE-----
    MIIGPTCCBCWgAwIBAgIBAjANBgkqhkiG9w0BAQsFADBdMQswCQYDVQQGEwJGUjEP
    MA0GA1UECgwGT3JhbmdlMRgwFgYDVQQLDA9GUiA4OSAzODAxMjk4NjYxIzAhBgNV
    BAMMGk9yYW5nZSBJbnRlcm5hbCBHMiBSb290IENBMB4XDTE2MDUwMzEzMjA0NFoX
    DTM2MDUwMzEzMjA0NFowXzELMAkGA1UEBhMCRlIxDzANBgNVBAoMBk9yYW5nZTEY
    MBYGA1UECwwPRlIgODkgMzgwMTI5ODY2MSUwIwYDVQQDDBxPcmFuZ2UgSW50ZXJu
    YWwgRzIgU2VydmVyIENBMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA
    mPYx/p4jDiGaWI25AolfgwC2p/fI209Jjx++FfKnFb9OfFC66Wuny2ziA+2uuPXW
    OI118DGfhfS45dyf/BYo2v70kMiIvZI1FN5pcaCDpYSTxGmWxcaiVm8BIzFIT7HQ
    gK61jYeJrsTrzoUYAgZVnF+pZZgb04Sy+SdVctNWVKKuXfx59XiXqpq2WSwULb8Z
    lYHsFWUkjJxpaPYYzth1BPoxbYdTcf2alyoCsphDuKySMkClDSFWqAZVXJmdAQ2H
    wEUvgLt7cpAYCPXas+9VOceTIG8XAZu53goML52l7ueHqY435ZCOeZOLFqmNUEGY
    9zV1K/4jJDyo0K5/K6xzcgRgs1KyWjT/2lJAsEG7j7P1r1KtYpjZy1860JB9nM8w
    YP0O6sKlHod2jCQObAznrKRNnYUUGYlkdXp/M6MejSPrQtoua/NtSn5h9FBHymvw
    GBYtljx3srsQTO6pwxE653h/4QouIZ3Kf7Twmw6CLS53ZsleplpBcZ4zm6K68Do/
    8ZRvfnIr1hk980LmIzf4aevALx/eCVB4HzspzOVq8CExCVg5BXdV4j9BSHmH6rV8
    PYAMdxXLzyt9uWMOVgQjJnWg4q6H8RcjJ/1NkAGoCOgFuaJu0f/jSyokfHr3Crio
    FZ7zpZ67cs7Dyz4ZfsEDV1om7WjsiQzR4tZSp6swXXMCAwEAAaOCAQQwggEAMA8G
    A1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMB0GA1UdDgQWBBS8PeSkx7yL
    MQtV4sMHoqL429e7cDAfBgNVHSMEGDAWgBTGON+k2onDjBtKS2UHJa+z1LZHgjCB
    hAYDVR0fBH0wezA+oDygOoY4aHR0cDovL3BraS1jcmwuaXRuLmZ0Z3JvdXAvY3Js
    L29yYW5nZWludGVybmFsZzItcm9vdC5jcmwwOaA3oDWGM2h0dHA6Ly9wa2kub3Jh
    bmdlLmNvbS9jcmwvb3JhbmdlaW50ZXJuYWxnMi1yb290LmNybDAWBgNVHSAEDzAN
    MAsGCSqBegEQDAoBATANBgkqhkiG9w0BAQsFAAOCAgEAQzrAaBHlbm8sFTNaLCm9
    0imjsDrpCriwTAV3sxaFMXxuoQOIN7frc+tulEq1ATZNicbiRqHJq9BnIc1MGu/A
    hzxvFMBAjBnj8/wqAY0GsFr52axYQHwgANs9OIuSw8oz3/z1/6T/42JtK+WlIS/r
    malTSJqa2CEe/fRVQJp5YcO0U1mIVHatcMq7Z9YVY6K8A7hb7qSs7GO36U7hStFt
    72s5++fdG+s5IDi2l6CMTZmelbhjsxJL/A9u8yrFJxniAHs5+ikwO+MoL2ZBC+0A
    Jt8Xtjye32//LGAyvai1jJ4U501wjBvvdbul1xVARV/sHrYSE+hvDEtfHcSn92H4
    zC9C2ANMGGxykbDLWbVfqmGW5Iz4UWCEv6n451QckMJNJI7743qeUc0AHdMtPDtx
    +ItAxpaUIXINR9Lqo3erWefYLSne7vi86QdqCvwHWpzjuIJ9DXOT5qRCN0M859AA
    ZFRBQn5zjPO1tyvtwb9/0o1AjL5NAMc88qQAmxkiC9/QzDL7LYxjKgkwCWx+DfJF
    /2Xe9zb3zGvT6xOjIELMQhNwnU37EMe4hSnn4dJBf0yNq4g3Rt3MmFFCDy6YE3PG
    rtfQ/CUwLPJb9Bqroqe4ryYPsVKpKe+71fEOKS8iHlFSggGZucZ63WKSyiBS1sJt
    V5qME0d/UK4/+4FTP7FcXmc=
    -----END CERTIFICATE-----
```

## Variants

The default SonarQube template is designed to work on untagged runners, without any proxy configuration using Docker images
from the internet and using Default Trusted Certificate Authorities.

Nevertheless there are template variants available to cover specific cases.

### DevOps Store variant

This variant:

* declares Orange G2 CA certificates (as `CUSTOM_CA_CERTS` variable),
* uses DevOps Store runners,
* configures http proxy accordingly,
* uses Docker images through the [DevOps Store Artifactory mirror](https://artifactory-cwfr1.rd.francetelecom.fr/).

_(in other words, using this variant will revert to pretty much pre-`1.0.0` situation)_

If you wish to use it, add the following to your  `gitlab-ci.yml`:

```yaml
include:
  # main template
  - project: 'gitlab-ci-templates/sonar'
    ref: '1.0.0'
    file: '/templates/gitlab-ci-sonar.yml'
  # DevOps Store variant
  - project: 'gitlab-ci-templates/sonar'
    ref: '1.0.0'
    file: '/templates/gitlab-ci-sonar-dos.yml'
```


## Version history

See [Tags page](https://gitlab.tech.orange/gitlab-ci-templates/sonar/-/tags)

